select * from public.advisors_responses

-- VERIFICA PATROMONIO
select collaborator_id,lm.data_date,sum(net)net from advisors_patrimony_year_history ap 
join lasts_days_by_month lm ON (ap.data_date = lm.data_date)
where collaborator_id = '013ad726-4f31-11ea-b085-02ca363b1808'
group by collaborator_id,lm.data_date
order by lm.data_date DESC

select * from collaborators
where name like 'Bruno%'

--Bruno Silva
select * from public.advisors_details
where name = 'Bruno de Nunes Silva' -- 51479.7277 net-income

select * from advisors_customer_count_history ac
where collaborator_id = '27c3c09e-1932-11eb-bcc3-02583319e176'
order by data_date DESC

select distinct c.id,c.name,c.category,lm.data_date,sum(cr.gross_revenue)gross_revenue,
sum(net_revenue)net_revenue,sum(cr.net_income)net_income,ac.customer_count
from collaborators c
left join customers_revenues cr on cr.collaborator_id = c.id
JOIN lasts_days_by_month lm ON ((lm.data_date = cr.data_date))
LEFT JOIN advisors_customer_count_history ac ON (((ac.collaborator_id = c.id) AND (ac.data_date = lm.data_date)))
where c.name = 'Amanda Leite Lopes Caputo' --and cr.product = 'Previdencia'
and (lm.data_date >= (CURRENT_DATE - '1 year'::interval))
group by c.id,c.name,lm.data_date,ac.customer_count
order by c.name asc,lm.data_date desc --3846.7664 --> bi

--Verifica só na tabela customers_revenues
select collaborator_id,lm.data_date,sum(net_income) from customers_revenues cr
JOIN lasts_days_by_month lm ON ((lm.data_date = cr.data_date))
where collaborator_id = '013ad726-4f31-11ea-b085-02ca363b1808'
and (lm.data_date >= (CURRENT_DATE - '1 year'::interval))
group by collaborator_id,lm.data_date
order by lm.data_date DESC

