import psycopg2
import csv
import urllib.request
import io

#conectando no banco postgres
con = psycopg2.connect(
            host = "localhost",
            database="postgres",
            user = "postgres",
            password = "postgres")

#cursor 
cur = con.cursor()

# cria tabela info_diario se não existir... Se existir trunca os dados para uma nova insercao..
cur.execute("""CREATE TABLE IF NOT EXISTS info_diario (cnpj_fundo text,dt_comp date,vl_total text,
vl_quota text,vl_patri text,cap_dia text,resg_dia text,nr_cot text);
     truncate table info_diario;
""")

#lendo arquivo csv
def csv_import(url):
    url_open = urllib.request.urlopen(url)
    csvfile = csv.reader(io.TextIOWrapper(url_open,encoding='UTF-8'), delimiter=';') 
    return csvfile;
csv_read = csv_import('http://dados.cvm.gov.br/dados/FI/DOC/INF_DIARIO/DADOS/inf_diario_fi_202012.csv')

# percorre os dados CSV do site e ignora o cabecalho
next(csv_read,None)
for row in csv_read:
 #print(row)
 
 cnpj_fundo = row[0]
 dt_comp = row[1]
 vl_total = row[2]
 vl_quota = row[3]
 vl_patri = row[4]
 cap_dia = row[5]
 resg_dia = row[6]
 nr_cot = row[7]

#faz insert dos dados na tabela info_diario
 cur.execute('''insert INTO info_diario (cnpj_fundo,dt_comp,vl_total,vl_quota,vl_patri,cap_dia,resg_dia,nr_cot)
      values (%s,%s,%s,%s,%s,%s,%s,%s)''', (cnpj_fundo,dt_comp,vl_total,vl_quota,vl_patri,cap_dia,resg_dia,nr_cot))
      #excluindo o cabecalho inserido
#cur.execute('''delete from info_diario where cnpj_fundo in (select cnpj_fundo
      #from info_diario where cnpj_fundo = 'CNPJ_FUNDO')''')

#commit the transcation 
con.commit()
#close the cursor
cur.close()
#close the connection
con.close()
