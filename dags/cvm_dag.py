from airflow.models import DAG
from airflow.utils.dates import days_ago, timedelta
from datetime import datetime
from airflow.operators.python_operator import PythonOperator, BranchPythonOperator
from airflow.operators.bash_operator import BashOperator


args={
    'owner': 'Alex',
    'start_date':  days_ago(1), #datetime(2020,12,24,10,00),
    'depends_on_past': False,
    'email': ['lefranklex@gmail.com'],
    'email_on_failure':True,
    'email_on_retry': True,
    'retries':1,
    'retry_delay': timedelta(minutes=5)
}

dag = DAG(dag_id='CVM_dag',default_args=args, schedule_interval="@daily")

# vai ate a pasta onde esta o arquivo python e executa a insercao
find_py = BashOperator(
    task_id='file_go',
    bash_command= "python3 /Users/stark/PROJETO_CVM/dags/insert_datas_bd.py ",
    dag=dag
)

#confirma se os dados foram inseridos
status = BashOperator(
    task_id='confirma',
    bash_command= 'echo "DADOS INSERIDOS COM SUCESSO"',
    dag=dag
)

find_py >> status


